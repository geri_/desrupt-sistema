import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
import {FormControl, Validators, FormGroup, FormBuilder} from "@angular/forms";
import * as $ from 'jquery';

//Classes
import { Usuario } from './class/usuario';

//Services
import { SignService } from './sign.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit, AfterContentChecked {
  public modalAtivo: string;
  private username: Usuario = new Usuario();
  private password: string;
  private logado: string;
  private formulario: FormGroup;

  constructor(private generalService: GeneralService, private signService: SignService, private formBuilder: FormBuilder) { 
    this.formulario = this.formBuilder.group({
      email: [null, [Validators.email, Validators.required]],
      senha: [null, [Validators.minLength(6), Validators.required, Validators.maxLength(20)]]
    })
  }

  verificaValidTouched(campo){
    return (this.formulario.get(campo).invalid && (this.formulario.get(campo).touched || this.formulario.get(campo).dirty) ) ? 'has-erro has-feedback' : '';
  }

  ngOnInit() {
    window.sessionStorage.setItem('logado', 'false');
    this.generalService.background("url('background.jpg')");
    this.generalService.background_color("#9b59b6");
  }
  public abrirModal(modal) {
    this.generalService.abrirModal(modal);
  }
  fazerLogin(){
    this .signService.fazerLogin(this.username);
  }
  ngAfterContentChecked(){
    this.modalAtivo = this.generalService.modalAtivo;
    // this.username = this.generalService.username;
    // this.password = this.generalService.password;
    this.logado = this.generalService.logado;
  }
}
