import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { WhiteLabelComponent } from './criar-paginas/white-label/white-label.component';
import { LandingPagesComponent } from './criar-paginas/landing-pages/landing-pages.component';
import { StaticPagesComponent } from './criar-paginas/static-pages/static-pages.component';
import { ConfiguracoesComponent } from './configuracoes-page/configuracoes/configuracoes.component';
import { ConteudoComponent } from './conteudo.component';
import { MenuModule } from '../../menus/menu.module';
import { FicCientificaComponent } from './ficcao/fic-cientifica/fic-cientifica.component';


@NgModule({
  declarations: [
    WhiteLabelComponent,
    LandingPagesComponent,
    StaticPagesComponent,
    ConfiguracoesComponent,
    ConteudoComponent,
    FicCientificaComponent
  ],
  exports: [
    ConteudoComponent
  ],
  imports: [
    CommonModule,
    MenuModule
  ]
})
export class ConteudoModule { }
