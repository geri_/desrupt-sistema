import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';

@Component({
  selector: 'app-fic-cientifica',
  templateUrl: './fic-cientifica.component.html',
  styleUrls: ['./fic-cientifica.component.less']
})
export class FicCientificaComponent implements OnInit, AfterContentChecked {
  public select: string;
  public aba: string;
  public modalAtivo: string;

  constructor(private generalService: GeneralService) { }

  ngOnInit() {
  }
  public abrirAba(aba_v) {
    this.generalService.abas(aba_v);
  }
  public nmArquivo(file) {
    this.generalService.nmArquivo(file);
  }
  public previewImg(imagem) {
    this.generalService.previewImg(imagem);
  }
  public abrirModal(modal) {
    this.generalService.abrirModal(modal);
  }
  ngAfterContentChecked(){
    this.select = this.generalService.select;
    this.aba = this.generalService.abas_select;
    this.modalAtivo = this.generalService.modalAtivo;
  }
}
