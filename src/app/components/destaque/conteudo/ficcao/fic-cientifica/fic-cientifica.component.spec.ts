import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicCientificaComponent } from './fic-cientifica.component';

describe('FicCientificaComponent', () => {
  let component: FicCientificaComponent;
  let fixture: ComponentFixture<FicCientificaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicCientificaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicCientificaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
