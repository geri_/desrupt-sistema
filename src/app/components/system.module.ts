import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Modules
import { ConteudoModule } from '../components/destaque/conteudo/conteudo.module';
import { CommonsModule } from '../commons/commons.module';
import { ModaisModule } from '../modais/modais.module';
import { MenuModule } from '../components/menus/menu.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ConteudoModule,
    CommonsModule,
    ModaisModule,
    MenuModule
  ],
  exports:[
    CommonModule,
    ConteudoModule,
    CommonsModule,
    ModaisModule,
    MenuModule
  ]
})
export class SystemModule { }
