import { Injectable } from '@angular/core';
import * as $ from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  public sub_menu: boolean;
  public menu: string;
  public select: string;
  public abas_select: string;
  public modalAtivo: string;
  private username: string = 'alansantos';
  private password: string = '123alan';
  public logado: string;
  public backgroundBody: string;
  public backgroundColorBody: string;

  constructor() {

  }
  public showSubMenu(identificador){      
    this.sub_menu = true;
    this.menu = identificador;
  }  
  public hideSubMenu(){
    this.sub_menu = false;
    this.menu = "";
  }

  public onselect(valor){
    this.select = valor;
  }
  public abrirModal(modal){
    this.modalAtivo = modal;
  }
  public abas(aba_v){
    this.abas_select = aba_v;
  }
  public background(bgBody){
    this.backgroundBody = bgBody;
  }
  public background_color(bgColorBody){
    this.backgroundColorBody = bgColorBody;
  }
  public nmArquivo(id){
    var alvo = $(id);
    var alvo_next = $(alvo).next();
    var inputNome = $(alvo).prev();
    var inputFicheiro = alvo_next;
    $(inputFicheiro).on('change', function() {
      var nome = $(inputFicheiro).val().split("\\").pop();
      $(inputNome).text(nome);
      $(inputNome).html(nome);
    })
  };
public previewImg(imagem){
    var labelLocation = imagem;
    var inputFileLocation = $(labelLocation).next();
    $(inputFileLocation).on('change', function(){
      var imgItem = $(this)[0].files;
      var imgCount = $(this)[0].files.length;
      var imgPath = $(this)[0].value;
      var imgExt = imgPath.substring(imgPath.lastIndexOf('.')+1).toLowerCase();
      var imagePreview = $(".imgpreview");
      var labelImage = imagem;
      var imagePreview2 = $(labelImage).closest(".x-fileLabel");
      var imagePreview3 = $(imagePreview2).find(".x-imagePreview");
      imagePreview.empty();
      if(imgExt == "gif" || imgExt == "png" || imgExt == "jpg" || imgExt == "jpeg" || imgExt == "bmp"){
        if(typeof(FileReader) != "undefined"){
          for (var i = 0; i < imgCount; i++){
            var reader = new FileReader();
            reader.onload = function(e){
              let fileUrl = (<FileReader>event.target).result;
              $("<img />", {
                "src": fileUrl,
                "width": "150",
                "class": "imgClass"
              }).appendTo(imagePreview3);
            }
            imagePreview2.show();
            reader.readAsDataURL($(this)[0].files[i]);
          }
        }else{
          imagePreview2.html("Browser ini tidak mendukung FileReader");
        }
      } else{
        imagePreview2.html("File harus format gambar");
      }
    });
  }
}
