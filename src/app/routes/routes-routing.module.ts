import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from '../login/login.component';
import { ConteudoComponent } from '../components/destaque/conteudo/conteudo.component';

const routes: Routes = [
      {path: '', component: LoginComponent},
      {path: 'home', component: ConteudoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutesRoutingModule { }