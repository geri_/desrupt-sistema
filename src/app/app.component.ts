import { Component,  OnInit, AfterContentChecked } from '@angular/core';

//services
import { SignService } from './login/sign.service';
import { GeneralService } from 'src/app/principal/general.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterContentChecked {
  public title = 'CMS-v2';
  public mostrarMenu: boolean = false;
  public backgroundBody: string;
  public backgroundColorBody: string;
  
  constructor(private signService: SignService, private generalService: GeneralService) { }

  ngOnInit() {
    this.signService.mostrarMenuEmitter.subscribe(
      mostrar => this.mostrarMenu = mostrar
    )
  }
  ngAfterContentChecked(){
    // this.backgroundBody = this.generalService.backgroundBody;
    // this.backgroundColorBody = this.generalService.backgroundColorBody;
  }
}
